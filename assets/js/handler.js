/* ======================================
   =  Some handlers functions		    =
   ====================================== 
*/	
$(document).ready(function(){
	// _________This part is responsible to active student porfile navs___
	$( document ).on('mouseover','.navs-li',function(event){
		var obj = $(this).children().eq(0);
		obj.addClass('active');
	})
	$( document ).on('mouseout','.navs-li',function(event){
		var obj = $(this).children().eq(0);
		obj.removeClass('active');
	})
	// _________This part is responsible to active  nav tabs___
	$( document ).on('click','.navs-li',function(event){
		var this_tab = $(this).attr('link-to');
		var tabs = $('.usertab');
		tabs.removeClass('in')
		tabs.hide();
		$(this_tab).show('slow')
		$(this_tab).addClass('in')
	})
	// _________This part is responsible to active mail box list options___
	$( document ).on('mouseover','.mailbox-lists',function(event){
		var obj = $(this).children().eq(1);
		obj.addClass('active');
	})
	$( document ).on('mouseout','.mailbox-lists',function(event){
		var obj = $(this).children().eq(1);
		obj.removeClass('active');
	})
	$( document ).on('click','.mailbox-lists',function(event){
		var This = $(this);
		var icon = $(this).children().eq(1);
		var this_tab = $(this).attr('link-to');
		console.log(icon);
		$('.mailbox-icon').removeClass('sx');
		$('.mailbox-lists').removeClass('active');
		This.addClass('active');
		icon .addClass('sx');
		var tabs = $('.mail-tab');
		tabs.removeClass('in');
		tabs.hide();
		$(this_tab).show('slow')
		$(this_tab).addClass('in')
	})
	$( document ).on('click','.show-full-text',function(event){
		var This = $(this);
		This.toggleClass('fa-chevron-down');
		This.closest('.mail-row').find('.mail-text').toggleClass('active');
		
	})
	// ===================================
	$( document ).on('click','.exam-lists',function(event){
		var This = $(this);
		var this_tab = $(this).attr('link-to');
		$('.exam-lists').removeClass('active');
		This.addClass('active');
		var tabs = $('.exam-tab');
		tabs.removeClass('in');
		tabs.hide();
		$(this_tab).show('slow')
		$(this_tab).addClass('in')
	})
})
function slideChecker(pannel,event)
{
	event.stopPropagation();
	if (pannel.hasClass('active')) {
		pannel.removeClass('active');
		pannel.slideUp();
	}
	else {
		$('.med-pannel').slideUp();
		$('.med-pannel').removeClass('active');
		pannel.slideDown();
		pannel.addClass('active');
	}
	return false; // cause this btn do not slide up all pannels
}
