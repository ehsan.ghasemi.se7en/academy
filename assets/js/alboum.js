/* ======================================
  ~ Author : Ehsan Ghasemi
  ~ Date : 10/29/2016
  ~ Licence : petra design
  ~ Version : 1.0.0
  ~ Email : ehs.ghasemi@gmail.com
   ====================================== 
*/	
$(document).ready(function(){
	$(document).on('click','#alboum-cat-btn',function(event){
		$('.alboum-cat-pannel').slideToggle();
	})
})